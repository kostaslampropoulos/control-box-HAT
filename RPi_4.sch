EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR019
U 1 1 6072DB2A
P 6800 4025
AR Path="/607284F0/6072DB2A" Ref="#PWR019"  Part="1" 
AR Path="/6102AFA0/6072DB2A" Ref="#PWR?"  Part="1" 
F 0 "#PWR019" H 6800 3775 50  0001 C CNN
F 1 "GND" H 6805 3852 50  0000 C CNN
F 2 "" H 6800 4025 50  0001 C CNN
F 3 "" H 6800 4025 50  0001 C CNN
	1    6800 4025
	1    0    0    -1  
$EndComp
$Comp
L Connector:Raspberry_Pi_2_3 J2
U 1 1 60733CC4
P 6850 2550
AR Path="/607284F0/60733CC4" Ref="J2"  Part="1" 
AR Path="/6102AFA0/60733CC4" Ref="J?"  Part="1" 
F 0 "J2" H 7825 3975 50  0000 C CNN
F 1 "Raspberry_Pi 3b" H 7850 3900 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 6850 2550 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 6850 2550 50  0001 C CNN
F 4 "-" H 6850 2550 50  0001 C CNN "Mfr. "
F 5 "-" H 6850 2550 50  0001 C CNN "Mfr. No. "
	1    6850 2550
	1    0    0    -1  
$EndComp
NoConn ~ 6950 1250
NoConn ~ 7050 1250
NoConn ~ 6050 1650
NoConn ~ 6050 1750
NoConn ~ 7650 3250
Wire Wire Line
	6450 3950 6450 3850
Wire Wire Line
	7650 2450 7750 2450
Text HLabel 7750 3050 2    50   Input ~ 0
drogue_signal
Wire Wire Line
	7750 3050 7650 3050
Wire Wire Line
	5950 2350 6050 2350
Text HLabel 5950 3150 0    50   Input ~ 0
vc_signal
Wire Wire Line
	7650 2350 7750 2350
Text HLabel 7750 3350 2    50   Input ~ 0
fire_signal
NoConn ~ 6050 2550
NoConn ~ 7650 2750
NoConn ~ 7650 2650
Wire Wire Line
	4100 5250 4200 5250
Wire Wire Line
	4100 5300 4200 5300
Wire Wire Line
	4100 5350 4200 5350
Wire Wire Line
	4100 5400 4200 5400
Wire Wire Line
	4100 5500 4200 5500
Wire Wire Line
	4100 5550 4200 5550
Wire Wire Line
	4100 5700 4200 5700
Wire Wire Line
	5825 5650 5925 5650
Text Label 3325 5400 2    30   ~ 0
vc_ok
Text Label 3325 5300 2    30   ~ 0
fire_ok
Text Label 3325 5150 2    30   ~ 0
drogue_ok
Text Label 3325 5200 2    30   ~ 0
abort_ok
Text Label 3325 5050 2    30   ~ 0
reserved_ok
Text Label 3325 5350 2    30   ~ 0
fill_ok
Text Label 3325 5250 2    30   ~ 0
approach_ok
Wire Wire Line
	5825 5050 5925 5050
Wire Wire Line
	5925 5250 5825 5250
Wire Wire Line
	5925 5450 5825 5450
Wire Wire Line
	5925 5350 5825 5350
Wire Wire Line
	5825 5150 5925 5150
Wire Wire Line
	5925 4950 5825 4950
Text HLabel 5950 2350 0    50   Input ~ 0
fill_signal
Wire Wire Line
	5950 3150 6050 3150
NoConn ~ 6050 1950
NoConn ~ 6050 2150
NoConn ~ 6050 2450
NoConn ~ 6050 2850
NoConn ~ 6050 2950
Wire Wire Line
	5825 5550 5925 5550
$Comp
L power:+5V #PWR0106
U 1 1 60C97572
P 6700 1075
AR Path="/607284F0/60C97572" Ref="#PWR0106"  Part="1" 
AR Path="/6102AFA0/60C97572" Ref="#PWR?"  Part="1" 
F 0 "#PWR0106" H 6700 925 50  0001 C CNN
F 1 "+5V" H 6600 1175 50  0000 C CNN
F 2 "" H 6700 1075 50  0001 C CNN
F 3 "" H 6700 1075 50  0001 C CNN
	1    6700 1075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1075 6700 1200
$Comp
L Control_Box:PCA9956BTW U3
U 1 1 60D144EE
P 3700 5150
AR Path="/607284F0/60D144EE" Ref="U3"  Part="1" 
AR Path="/6102AFA0/60D144EE" Ref="U?"  Part="1" 
F 0 "U3" H 3750 5675 50  0000 C CNN
F 1 "PCA9956BTW" H 3750 5600 50  0000 C CNN
F 2 "Control_Box:PCA9956BTWY" H 3800 5150 50  0001 C CNN
F 3 "" H 3800 5150 50  0001 C CNN
F 4 "NXP Semiconductors" H 3700 5150 50  0001 C CNN "Mfr. "
F 5 "PCA9956BTWY" H 3700 5150 50  0001 C CNN "Mfr. No. "
	1    3700 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3325 5150 3400 5150
Wire Wire Line
	3325 5200 3400 5200
Wire Wire Line
	3325 5050 3400 5050
$Comp
L power:GND #PWR015
U 1 1 60D49A1A
P 3750 5950
AR Path="/607284F0/60D49A1A" Ref="#PWR015"  Part="1" 
AR Path="/6102AFA0/60D49A1A" Ref="#PWR?"  Part="1" 
F 0 "#PWR015" H 3750 5700 50  0001 C CNN
F 1 "GND" H 3755 5777 50  0000 C CNN
F 2 "" H 3750 5950 50  0001 C CNN
F 3 "" H 3750 5950 50  0001 C CNN
	1    3750 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5950 3750 5850
$Comp
L power:GND #PWR013
U 1 1 60D4E978
P 2875 5725
AR Path="/607284F0/60D4E978" Ref="#PWR013"  Part="1" 
AR Path="/6102AFA0/60D4E978" Ref="#PWR?"  Part="1" 
F 0 "#PWR013" H 2875 5475 50  0001 C CNN
F 1 "GND" H 2880 5552 50  0000 C CNN
F 2 "" H 2875 5725 50  0001 C CNN
F 3 "" H 2875 5725 50  0001 C CNN
	1    2875 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	2875 5650 3400 5650
Wire Wire Line
	2875 5650 2875 5450
Wire Wire Line
	2875 5450 3400 5450
Connection ~ 2875 5650
$Comp
L power:GND #PWR016
U 1 1 60D523D9
P 4575 5650
AR Path="/607284F0/60D523D9" Ref="#PWR016"  Part="1" 
AR Path="/6102AFA0/60D523D9" Ref="#PWR?"  Part="1" 
F 0 "#PWR016" H 4575 5400 50  0001 C CNN
F 1 "GND" H 4580 5477 50  0000 C CNN
F 2 "" H 4575 5650 50  0001 C CNN
F 3 "" H 4575 5650 50  0001 C CNN
	1    4575 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4575 5450 4575 5650
Connection ~ 4575 5650
Wire Wire Line
	4100 5650 4575 5650
Wire Wire Line
	3400 4900 2875 4900
Wire Wire Line
	2875 4900 2875 5100
Connection ~ 2875 5450
$Comp
L Device:R R3
U 1 1 60DE0FAB
P 2650 4950
AR Path="/607284F0/60DE0FAB" Ref="R3"  Part="1" 
AR Path="/6102AFA0/60DE0FAB" Ref="R?"  Part="1" 
F 0 "R3" V 2575 4875 50  0000 L CNN
F 1 "220" V 2650 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2580 4950 50  0001 C CNN
F 3 "~" H 2650 4950 50  0001 C CNN
F 4 "ROHM Semiconductor" H 2650 4950 50  0001 C CNN "Mfr. "
F 5 "SDR03EZPJ221" H 2650 4950 50  0001 C CNN "Mfr. No. "
	1    2650 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 5100 2875 5100
Connection ~ 2875 5100
Wire Wire Line
	2875 5100 2875 5450
Wire Wire Line
	2875 5650 2875 5725
Wire Wire Line
	2650 4800 3400 4800
Wire Wire Line
	3400 4950 3325 4950
Wire Wire Line
	3400 4850 3325 4850
Text Label 3325 4950 2    28   ~ 0
AD2
Text Label 3325 4850 2    28   ~ 0
AD0
Text Label 3150 4600 2    28   ~ 0
AD0
Text Label 3150 4675 2    28   ~ 0
AD2
$Comp
L power:+5V #PWR014
U 1 1 60DEF259
P 3600 4400
AR Path="/607284F0/60DEF259" Ref="#PWR014"  Part="1" 
AR Path="/6102AFA0/60DEF259" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 3600 4250 50  0001 C CNN
F 1 "+5V" H 3500 4500 50  0000 C CNN
F 2 "" H 3600 4400 50  0001 C CNN
F 3 "" H 3600 4400 50  0001 C CNN
	1    3600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4600 3200 4600
Wire Wire Line
	3150 4675 3200 4675
Wire Wire Line
	3200 4675 3200 4600
Connection ~ 3200 4600
Wire Wire Line
	3200 4600 3600 4600
NoConn ~ 4100 5050
NoConn ~ 4100 5100
NoConn ~ 4100 5150
NoConn ~ 4100 5200
Text Label 3225 5000 2    28   ~ 0
~OE
Wire Wire Line
	3225 5000 3400 5000
Wire Wire Line
	7775 2050 7650 2050
Wire Wire Line
	7775 1950 7650 1950
NoConn ~ 6050 3050
Text HLabel 7750 2350 2    50   Input ~ 0
abort_signal
NoConn ~ 7650 2250
NoConn ~ 6050 3250
NoConn ~ 6050 2750
Wire Wire Line
	6450 3950 6550 3950
Wire Wire Line
	7150 3950 7150 3850
Wire Wire Line
	6550 3850 6550 3950
Connection ~ 6550 3950
Wire Wire Line
	6550 3950 6650 3950
Wire Wire Line
	6650 3950 6650 3850
Connection ~ 6650 3950
Wire Wire Line
	6650 3950 6750 3950
Wire Wire Line
	6750 3850 6750 3950
Connection ~ 6750 3950
Wire Wire Line
	6750 3950 6800 3950
Wire Wire Line
	6850 3850 6850 3950
Connection ~ 6850 3950
Wire Wire Line
	6950 3850 6950 3950
Wire Wire Line
	6850 3950 6950 3950
Connection ~ 6950 3950
Wire Wire Line
	7050 3850 7050 3950
Wire Wire Line
	6950 3950 7050 3950
Connection ~ 7050 3950
Wire Wire Line
	7050 3950 7150 3950
Wire Wire Line
	6800 4025 6800 3950
Connection ~ 6800 3950
Wire Wire Line
	6800 3950 6850 3950
Wire Wire Line
	6650 1250 6650 1200
Wire Wire Line
	6650 1200 6700 1200
Wire Wire Line
	6750 1250 6750 1200
Wire Wire Line
	6750 1200 6700 1200
Connection ~ 6700 1200
$Comp
L power:+3V3 #PWR0107
U 1 1 60FDCB6B
P 3325 1250
AR Path="/607284F0/60FDCB6B" Ref="#PWR0107"  Part="1" 
AR Path="/6102AFA0/60FDCB6B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0107" H 3325 1100 50  0001 C CNN
F 1 "+3V3" H 3340 1423 50  0000 C CNN
F 2 "" H 3325 1250 50  0001 C CNN
F 3 "" H 3325 1250 50  0001 C CNN
	1    3325 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 60FE5D96
P 2950 1800
AR Path="/607284F0/60FE5D96" Ref="C16"  Part="1" 
AR Path="/6102AFA0/60FE5D96" Ref="C?"  Part="1" 
F 0 "C16" V 2698 1800 50  0000 C CNN
F 1 "100n" V 2789 1800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2988 1650 50  0001 C CNN
F 3 "~" H 2950 1800 50  0001 C CNN
F 4 "KEMET" H 2950 1800 50  0001 C CNN "Mfr. "
F 5 "C0603C104M4RAC" H 2950 1800 50  0001 C CNN "Mfr. No. "
	1    2950 1800
	0    1    1    0   
$EndComp
Connection ~ 3325 1800
Wire Wire Line
	3325 1800 3325 1950
Wire Wire Line
	3100 1800 3325 1800
Wire Wire Line
	2800 1800 2700 1800
Wire Wire Line
	2700 1800 2700 2150
Wire Wire Line
	2925 2150 2700 2150
Connection ~ 2700 2150
Wire Wire Line
	2700 2150 2700 2250
Wire Wire Line
	2925 2250 2700 2250
Connection ~ 2700 2250
Wire Wire Line
	2700 2250 2700 2350
Wire Wire Line
	2925 2350 2700 2350
Connection ~ 2700 2350
Wire Wire Line
	2700 2350 2700 2600
Wire Wire Line
	3325 2550 3325 2600
Wire Wire Line
	3325 2600 2700 2600
Connection ~ 2700 2600
Wire Wire Line
	2700 2600 2700 2650
$Comp
L power:GND #PWR0108
U 1 1 6100D4B0
P 2700 2750
AR Path="/607284F0/6100D4B0" Ref="#PWR0108"  Part="1" 
AR Path="/6102AFA0/6100D4B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0108" H 2700 2500 50  0001 C CNN
F 1 "GND" H 2705 2577 50  0000 C CNN
F 2 "" H 2700 2750 50  0001 C CNN
F 3 "" H 2700 2750 50  0001 C CNN
	1    2700 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 6100DA83
P 4100 2650
AR Path="/607284F0/6100DA83" Ref="J9"  Part="1" 
AR Path="/6102AFA0/6100DA83" Ref="J?"  Part="1" 
F 0 "J9" H 4072 2532 50  0000 R CNN
F 1 "Conn_01x02_Male" H 4072 2623 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4100 2650 50  0001 C CNN
F 3 "~" H 4100 2650 50  0001 C CNN
F 4 "-" H 4100 2650 50  0001 C CNN "Mfr. "
F 5 "-" H 4100 2650 50  0001 C CNN "Mfr. No. "
	1    4100 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 2650 2700 2650
Connection ~ 2700 2650
Wire Wire Line
	2700 2650 2700 2750
Wire Wire Line
	3725 2350 3900 2350
Wire Wire Line
	3900 2350 3900 2550
$Comp
L Device:R R12
U 1 1 6101A02B
P 3900 1825
AR Path="/607284F0/6101A02B" Ref="R12"  Part="1" 
AR Path="/6102AFA0/6101A02B" Ref="R?"  Part="1" 
F 0 "R12" V 3825 1750 50  0000 L CNN
F 1 "1k" V 3900 1775 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3830 1825 50  0001 C CNN
F 3 "~" H 3900 1825 50  0001 C CNN
F 4 "ROHM Semiconductor" H 3900 1825 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPJ102" H 3900 1825 50  0001 C CNN "Mfr. No. "
	1    3900 1825
	1    0    0    -1  
$EndComp
Wire Wire Line
	3325 1250 3325 1625
Wire Wire Line
	3900 2350 3900 1975
Connection ~ 3900 2350
Wire Wire Line
	3900 1675 3900 1625
Wire Wire Line
	3900 1625 3325 1625
Connection ~ 3325 1625
Wire Wire Line
	3325 1625 3325 1800
$Comp
L Device:R R25
U 1 1 6103254C
P 4075 1825
AR Path="/607284F0/6103254C" Ref="R25"  Part="1" 
AR Path="/6102AFA0/6103254C" Ref="R?"  Part="1" 
F 0 "R25" V 4000 1750 50  0000 L CNN
F 1 "3.9k" V 4075 1750 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4005 1825 50  0001 C CNN
F 3 "~" H 4075 1825 50  0001 C CNN
F 4 "Bourns" H 4075 1825 50  0001 C CNN "Mfr. "
F 5 "CR0603-JW-392ELF" H 4075 1825 50  0001 C CNN "Mfr. No. "
	1    4075 1825
	1    0    0    -1  
$EndComp
$Comp
L Device:R R26
U 1 1 610326E5
P 4250 1825
AR Path="/607284F0/610326E5" Ref="R26"  Part="1" 
AR Path="/6102AFA0/610326E5" Ref="R?"  Part="1" 
F 0 "R26" V 4175 1750 50  0000 L CNN
F 1 "3.9k" V 4250 1750 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4180 1825 50  0001 C CNN
F 3 "~" H 4250 1825 50  0001 C CNN
F 4 "Bourns" H 4250 1825 50  0001 C CNN "Mfr. "
F 5 "CR0603-JW-392ELF" H 4250 1825 50  0001 C CNN "Mfr. No. "
	1    4250 1825
	1    0    0    -1  
$EndComp
Wire Wire Line
	4075 1675 4075 1625
Wire Wire Line
	4075 1625 3900 1625
Connection ~ 3900 1625
Wire Wire Line
	4250 1675 4250 1625
Wire Wire Line
	4250 1625 4075 1625
Connection ~ 4075 1625
Wire Wire Line
	3725 2150 4250 2150
Wire Wire Line
	4250 1975 4250 2150
Connection ~ 4250 2150
Wire Wire Line
	4250 2150 4450 2150
Wire Wire Line
	3725 2250 4075 2250
Wire Wire Line
	4075 1975 4075 2250
Connection ~ 4075 2250
Wire Wire Line
	4075 2250 4450 2250
Text Label 4450 2150 0    50   ~ 0
ID_SD
Text Label 4450 2250 0    50   ~ 0
ID_SC
Text Label 7800 1650 0    50   ~ 0
ID_SD
Text Label 7800 1750 0    50   ~ 0
ID_SC
Wire Wire Line
	7800 1650 7650 1650
Wire Wire Line
	7800 1750 7650 1750
$Comp
L astrohat:CAT24C32WI-GT3 U5
U 1 1 60FD7CC3
P 3325 2250
AR Path="/607284F0/60FD7CC3" Ref="U5"  Part="1" 
AR Path="/6102AFA0/60FD7CC3" Ref="U?"  Part="1" 
F 0 "U5" H 3575 2500 50  0000 C CNN
F 1 "M24C32-RMN6P" H 3325 1700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3325 2250 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21703d.pdf" H 3325 2250 50  0001 C CNN
F 4 "ON Semiconductor" H 3325 2250 50  0001 C CNN "Mfr. "
F 5 "CAT24C32WI-GT3" H 3325 2250 50  0001 C CNN "Mfr. No. "
	1    3325 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60DFFD58
P 4200 4650
AR Path="/607284F0/60DFFD58" Ref="R4"  Part="1" 
AR Path="/6102AFA0/60DFFD58" Ref="R?"  Part="1" 
F 0 "R4" V 4200 4900 50  0000 L CNN
F 1 "1.6k" V 4200 4575 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4130 4650 50  0001 C CNN
F 3 "~" H 4200 4650 50  0001 C CNN
F 4 "ROHM Semiconductor" H 4200 4650 50  0001 C CNN "Mfr. "
F 5 "SDR03EZPJ162" H 4200 4650 50  0001 C CNN "Mfr. No. "
	1    4200 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 60E06D8B
P 4375 4650
AR Path="/607284F0/60E06D8B" Ref="R5"  Part="1" 
AR Path="/6102AFA0/60E06D8B" Ref="R?"  Part="1" 
F 0 "R5" V 4375 4900 50  0000 L CNN
F 1 "1.6k" V 4375 4575 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4305 4650 50  0001 C CNN
F 3 "~" H 4375 4650 50  0001 C CNN
F 4 "ROHM Semiconductor" H 4375 4650 50  0001 C CNN "Mfr. "
F 5 "SDR03EZPJ162" H 4375 4650 50  0001 C CNN "Mfr. No. "
	1    4375 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 60E10090
P 4525 4650
AR Path="/607284F0/60E10090" Ref="R10"  Part="1" 
AR Path="/6102AFA0/60E10090" Ref="R?"  Part="1" 
F 0 "R10" V 4525 4900 50  0000 L CNN
F 1 "10k" V 4525 4575 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4455 4650 50  0001 C CNN
F 3 "~" H 4525 4650 50  0001 C CNN
F 4 "ROHM Semiconductor" H 4525 4650 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 4525 4650 50  0001 C CNN "Mfr. No. "
	1    4525 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 60E23555
P 4675 4650
AR Path="/607284F0/60E23555" Ref="R11"  Part="1" 
AR Path="/6102AFA0/60E23555" Ref="R?"  Part="1" 
F 0 "R11" V 4675 4900 50  0000 L CNN
F 1 "10k" V 4675 4575 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4605 4650 50  0001 C CNN
F 3 "~" H 4675 4650 50  0001 C CNN
F 4 "ROHM Semiconductor" H 4675 4650 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 4675 4650 50  0001 C CNN "Mfr. No. "
	1    4675 4650
	1    0    0    -1  
$EndComp
Text Label 4650 4950 2    28   ~ 0
~OE
Wire Wire Line
	4650 4950 4675 4950
Text HLabel 7750 2450 2    50   Input ~ 0
approach_signal
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 61DA3101
P 6125 5250
AR Path="/607385D5/61DA3101" Ref="J?"  Part="1" 
AR Path="/6102AF90/61DA3101" Ref="J?"  Part="1" 
AR Path="/607284F0/61DA3101" Ref="J1"  Part="1" 
F 0 "J1" H 6075 4750 50  0000 L CNN
F 1 "JST-PH 1x8" H 5900 4650 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S8B-PH-K_1x08_P2.00mm_Horizontal" H 6125 5250 50  0001 C CNN
F 3 "~" H 6125 5250 50  0001 C CNN
F 4 "Molex" H 6125 5250 50  0001 C CNN "Mfr. "
F 5 "53261-0771" H 6125 5250 50  0001 C CNN "Mfr. No. "
	1    6125 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5450 4575 5450
Wire Wire Line
	4575 5450 4575 5000
Wire Wire Line
	4100 5000 4575 5000
Connection ~ 4575 5450
Wire Wire Line
	3400 5250 3325 5250
Wire Wire Line
	3400 5300 3325 5300
Wire Wire Line
	3400 5350 3325 5350
Wire Wire Line
	3400 5400 3325 5400
Text Label 4200 5250 0    30   ~ 0
7seg_1
Text Label 4200 5300 0    30   ~ 0
7seg_2
Text Label 4200 5350 0    30   ~ 0
7seg_3
Text Label 4200 5400 0    30   ~ 0
7seg_4
Text Label 4200 5500 0    30   ~ 0
7seg_5
Text Label 4200 5550 0    30   ~ 0
7seg_6
Text Label 4200 5700 0    30   ~ 0
7seg_dot
NoConn ~ 3400 5500
NoConn ~ 3400 5550
NoConn ~ 3400 5600
NoConn ~ 3400 5700
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 61E45E13
P 7125 5250
AR Path="/607385D5/61E45E13" Ref="J?"  Part="1" 
AR Path="/6102AF90/61E45E13" Ref="J?"  Part="1" 
AR Path="/607284F0/61E45E13" Ref="J3"  Part="1" 
F 0 "J3" H 7100 4750 50  0000 L CNN
F 1 "JST-PH 1x8" H 6900 4650 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S8B-PH-K_1x08_P2.00mm_Horizontal" H 7125 5250 50  0001 C CNN
F 3 "~" H 7125 5250 50  0001 C CNN
F 4 "Molex" H 7125 5250 50  0001 C CNN "Mfr. "
F 5 "53261-0771" H 7125 5250 50  0001 C CNN "Mfr. No. "
	1    7125 5250
	1    0    0    -1  
$EndComp
Text Notes 5625 4825 0    50   ~ 0
State completion LEDs
Text Notes 6775 4825 0    50   ~ 0
7 segment display
Wire Notes Line
	6775 4750 6775 4825
Wire Notes Line
	6775 4825 7500 4825
Wire Notes Line
	7500 4825 7500 4750
Wire Notes Line
	7500 4750 6775 4750
Wire Notes Line
	5625 4750 5625 4825
Wire Notes Line
	5625 4825 6500 4825
Wire Notes Line
	6500 4825 6500 4750
Wire Notes Line
	6500 4750 5625 4750
Text Label 5825 4950 2    30   ~ 0
vc_ok
Text Label 5825 5150 2    30   ~ 0
fire_ok
Text Label 5825 5050 2    30   ~ 0
fill_ok
Text Label 5825 5250 2    30   ~ 0
approach_ok
Text Label 5825 5450 2    30   ~ 0
drogue_ok
Text Label 5825 5350 2    30   ~ 0
abort_ok
Text Label 5825 5550 2    30   ~ 0
main_ok
Text Label 5825 5650 2    30   ~ 0
reserved_ok
Wire Wire Line
	6925 4950 6825 4950
Wire Wire Line
	6925 5050 6825 5050
Wire Wire Line
	6925 5150 6825 5150
Wire Wire Line
	6925 5250 6825 5250
Wire Wire Line
	6925 5350 6825 5350
Wire Wire Line
	6925 5550 6825 5550
Wire Wire Line
	6925 5650 6825 5650
Text Label 6825 4950 2    30   ~ 0
7seg_1
Text Label 6825 5050 2    30   ~ 0
7seg_2
Text Label 6825 5150 2    30   ~ 0
7seg_3
Text Label 6825 5250 2    30   ~ 0
7seg_4
Text Label 6825 5350 2    30   ~ 0
7seg_5
Text Label 6825 5550 2    30   ~ 0
7seg_7
Text Label 6825 5650 2    30   ~ 0
7seg_dot
Text Label 6825 5450 2    30   ~ 0
7seg_6
Wire Wire Line
	6925 5450 6825 5450
Wire Wire Line
	7750 3350 7650 3350
Text HLabel 7750 2950 2    50   Input ~ 0
res_signal
Wire Wire Line
	7750 2950 7650 2950
NoConn ~ 6050 2050
Text Label 4375 4900 0    28   ~ 0
SCL
Text Label 4200 4850 0    28   ~ 0
SDA
Wire Wire Line
	4200 4850 4200 4800
Wire Wire Line
	4100 4850 4200 4850
Wire Wire Line
	3600 4400 3600 4425
Wire Wire Line
	3600 4425 4100 4425
Connection ~ 3600 4425
Wire Wire Line
	3600 4425 3600 4600
Connection ~ 4100 4425
Wire Wire Line
	4100 4425 4200 4425
Wire Wire Line
	4200 4500 4200 4425
Connection ~ 4200 4425
Wire Wire Line
	4200 4425 4375 4425
Wire Wire Line
	4375 4500 4375 4425
Connection ~ 4375 4425
Wire Wire Line
	4100 4425 4100 4800
Wire Wire Line
	4525 4950 4525 4800
Wire Wire Line
	4100 4950 4525 4950
Wire Wire Line
	4675 4500 4675 4425
Wire Wire Line
	4525 4500 4525 4425
Connection ~ 4525 4425
Wire Wire Line
	4525 4425 4675 4425
Wire Wire Line
	4375 4425 4525 4425
Wire Wire Line
	4675 4800 4675 4950
Wire Wire Line
	4375 4900 4375 4800
Wire Wire Line
	4100 4900 4375 4900
Text Label 7775 1950 0    50   ~ 0
SDA
Text Label 7775 2050 0    50   ~ 0
SCL
$Comp
L Device:C C?
U 1 1 61DFEC39
P 2175 5800
AR Path="/60723812/61DFEC39" Ref="C?"  Part="1" 
AR Path="/6102AF87/61DFEC39" Ref="C?"  Part="1" 
AR Path="/607284F0/61DFEC39" Ref="C17"  Part="1" 
F 0 "C17" H 2000 5875 50  0000 L CNN
F 1 "10u" H 1975 5725 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2213 5650 50  0001 C CNN
F 3 "~" H 2175 5800 50  0001 C CNN
F 4 "Taiyo Yuden" H 2175 5800 50  0001 C CNN "Mfr. "
F 5 "EMK107BBJ106MA-T" H 2175 5800 50  0001 C CNN "Mfr. No. "
	1    2175 5800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 61DFEC3F
P 2175 5600
AR Path="/607385D5/61DFEC3F" Ref="#PWR?"  Part="1" 
AR Path="/60723812/61DFEC3F" Ref="#PWR?"  Part="1" 
AR Path="/6102AF87/61DFEC3F" Ref="#PWR?"  Part="1" 
AR Path="/607284F0/61DFEC3F" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 2175 5450 50  0001 C CNN
F 1 "+3V3" H 2025 5675 50  0000 C CNN
F 2 "" H 2175 5600 50  0001 C CNN
F 3 "" H 2175 5600 50  0001 C CNN
	1    2175 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2175 5600 2175 5650
$Comp
L power:GND #PWR?
U 1 1 61DFEC46
P 2175 6000
AR Path="/607385D5/61DFEC46" Ref="#PWR?"  Part="1" 
AR Path="/60723812/61DFEC46" Ref="#PWR?"  Part="1" 
AR Path="/6102AF87/61DFEC46" Ref="#PWR?"  Part="1" 
AR Path="/607284F0/61DFEC46" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 2175 5750 50  0001 C CNN
F 1 "GND" H 2025 5925 50  0000 C CNN
F 2 "" H 2175 6000 50  0001 C CNN
F 3 "" H 2175 6000 50  0001 C CNN
	1    2175 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2175 6000 2175 5950
Text Notes 2750 5425 2    50   ~ 0
cap suggested by the \nled driver datasheet\n
Wire Notes Line
	1875 5275 2775 5275
Wire Notes Line
	2775 5275 2775 5425
Wire Notes Line
	2775 5425 1875 5425
Wire Notes Line
	1875 5425 1875 5300
Text HLabel 7750 2850 2    50   Input ~ 0
main_signal
Wire Wire Line
	7750 2850 7650 2850
Text Label 4200 5600 0    30   ~ 0
7seg_7
Wire Wire Line
	4100 5600 4200 5600
Wire Wire Line
	3325 5100 3400 5100
Text Label 3325 5100 2    30   ~ 0
main_ok
$EndSCHEMATC
