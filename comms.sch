EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR037
U 1 1 6075FA19
P 6700 4400
F 0 "#PWR037" H 6700 4150 50  0001 C CNN
F 1 "GND" H 6705 4227 50  0000 C CNN
F 2 "" H 6700 4400 50  0001 C CNN
F 3 "" H 6700 4400 50  0001 C CNN
	1    6700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4350 6600 4400
Wire Wire Line
	6600 4400 6700 4400
$Comp
L power:+3V3 #PWR011
U 1 1 60E84AC0
P 5800 3650
F 0 "#PWR011" H 5800 3500 50  0001 C CNN
F 1 "+3V3" H 5815 3823 50  0000 C CNN
F 2 "" H 5800 3650 50  0001 C CNN
F 3 "" H 5800 3650 50  0001 C CNN
	1    5800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3650 6200 3650
$Comp
L lsf-kicad:PQ9-Connector PQ1
U 1 1 6075F62A
P 6600 3750
AR Path="/6075F62A" Ref="PQ1"  Part="1" 
AR Path="/6075F4AB/6075F62A" Ref="PQ1"  Part="1" 
F 0 "PQ1" H 6948 3718 50  0000 L CNN
F 1 "PQ9-Connector" H 6948 3627 50  0000 L CNN
F 2 "Control_Box:PQ9-Connector_mod" H 7000 3250 50  0001 C CNN
F 3 "https://libre.space/pq9ish" H 7100 3950 50  0001 C CNN
	1    6600 3750
	1    0    0    -1  
$EndComp
NoConn ~ 6200 3950
NoConn ~ 6200 3850
NoConn ~ 6200 3750
NoConn ~ 6200 3550
NoConn ~ 6200 3450
NoConn ~ 6200 3350
$EndSCHEMATC
