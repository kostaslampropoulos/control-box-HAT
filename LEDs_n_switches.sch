EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5025 3450 5125 3450
Text HLabel 5125 3450 2    50   Input ~ 0
drogue_signal
Wire Wire Line
	5025 3150 5125 3150
$Comp
L power:GND #PWR0115
U 1 1 60CC19C0
P 4300 3775
AR Path="/607385D5/60CC19C0" Ref="#PWR0115"  Part="1" 
AR Path="/6102AF90/60CC19C0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0115" H 4300 3525 50  0001 C CNN
F 1 "GND" H 4305 3602 50  0000 C CNN
F 2 "" H 4300 3775 50  0001 C CNN
F 3 "" H 4300 3775 50  0001 C CNN
	1    4300 3775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3000 3650 3050
Wire Wire Line
	4400 3000 3650 3000
$Comp
L power:GND #PWR0114
U 1 1 60CB484C
P 3650 3050
AR Path="/607385D5/60CB484C" Ref="#PWR0114"  Part="1" 
AR Path="/6102AF90/60CB484C" Ref="#PWR?"  Part="1" 
F 0 "#PWR0114" H 3650 2800 50  0001 C CNN
F 1 "GND" H 3655 2877 50  0000 C CNN
F 2 "" H 3650 3050 50  0001 C CNN
F 3 "" H 3650 3050 50  0001 C CNN
	1    3650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5575 2875 5575 2850
Wire Wire Line
	5800 2875 5575 2875
Wire Wire Line
	5275 2550 5275 3000
Wire Wire Line
	5275 2550 5575 2550
Connection ~ 5275 2550
Wire Wire Line
	5275 2500 5275 2550
$Comp
L Device:C C13
U 1 1 60CA94E5
P 5575 2700
AR Path="/607385D5/60CA94E5" Ref="C13"  Part="1" 
AR Path="/6102AF90/60CA94E5" Ref="C?"  Part="1" 
F 0 "C13" H 5690 2746 50  0000 L CNN
F 1 "0.1u" H 5690 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5613 2550 50  0001 C CNN
F 3 "~" H 5575 2700 50  0001 C CNN
F 4 "KEMET" H 5575 2700 50  0001 C CNN "Mfr. "
F 5 "C0603C104M4RAC" H 5575 2700 50  0001 C CNN "Mfr. No. "
	1    5575 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5275 3000 5025 3000
$Comp
L power:+5V #PWR0113
U 1 1 60CA5386
P 5275 2500
AR Path="/607385D5/60CA5386" Ref="#PWR0113"  Part="1" 
AR Path="/6102AF90/60CA5386" Ref="#PWR?"  Part="1" 
F 0 "#PWR0113" H 5275 2350 50  0001 C CNN
F 1 "+5V" H 5290 2673 50  0000 C CNN
F 2 "" H 5275 2500 50  0001 C CNN
F 3 "" H 5275 2500 50  0001 C CNN
	1    5275 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3675 4400 3675
Wire Wire Line
	4300 3775 4300 3675
$Comp
L power:GND #PWR0112
U 1 1 60CA172B
P 5800 2875
AR Path="/607385D5/60CA172B" Ref="#PWR0112"  Part="1" 
AR Path="/6102AF90/60CA172B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0112" H 5800 2625 50  0001 C CNN
F 1 "GND" H 5805 2702 50  0000 C CNN
F 2 "" H 5800 2875 50  0001 C CNN
F 3 "" H 5800 2875 50  0001 C CNN
	1    5800 2875
	1    0    0    -1  
$EndComp
Text Label 4300 3375 2    50   ~ 0
abort_sig
Wire Wire Line
	4300 3075 4400 3075
Wire Wire Line
	5025 3075 5125 3075
Text HLabel 5125 3375 2    50   Input ~ 0
abort_signal
$Comp
L Connector_Generic:Conn_01x08 J5
U 1 1 60D54D89
P 7700 3525
AR Path="/607385D5/60D54D89" Ref="J5"  Part="1" 
AR Path="/6102AF90/60D54D89" Ref="J?"  Part="1" 
F 0 "J5" H 7780 3567 50  0000 L CNN
F 1 "JST-PH 1x8" H 7780 3476 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S8B-PH-K_1x08_P2.00mm_Horizontal" H 7700 3525 50  0001 C CNN
F 3 "~" H 7700 3525 50  0001 C CNN
F 4 "Molex" H 7700 3525 50  0001 C CNN "Mfr. "
F 5 "53261-0771" H 7700 3525 50  0001 C CNN "Mfr. No. "
	1    7700 3525
	1    0    0    -1  
$EndComp
Text Label 4300 3300 2    50   ~ 0
approach_sig
Text Label 4300 3450 2    50   ~ 0
drogue_sig
Wire Wire Line
	5025 3225 5125 3225
Wire Wire Line
	5025 3300 5125 3300
Wire Wire Line
	5025 3375 5125 3375
Wire Wire Line
	5025 3525 5125 3525
Wire Wire Line
	4400 3150 4300 3150
Wire Wire Line
	4400 3225 4300 3225
Wire Wire Line
	4400 3300 4300 3300
Wire Wire Line
	4400 3375 4300 3375
Wire Wire Line
	4400 3450 4300 3450
Wire Wire Line
	4300 3525 4400 3525
NoConn ~ 5025 3675
$Comp
L Control_Box:MAX6818 U4
U 1 1 60BDF5BC
P 4700 3400
AR Path="/60BDF5BC" Ref="U4"  Part="1" 
AR Path="/607385D5/60BDF5BC" Ref="U4"  Part="1" 
AR Path="/6102AF90/60BDF5BC" Ref="U?"  Part="1" 
F 0 "U4" H 4700 3000 50  0000 C CNN
F 1 "MAX6818" H 4712 3949 50  0000 C CNN
F 2 "Package_SO:SSOP-20_5.3x7.2mm_P0.65mm" H 4525 3350 50  0001 C CNN
F 3 "" H 4525 3350 50  0001 C CNN
F 4 "Maxim Integrated" H 4700 3400 50  0001 C CNN "Mfr. "
F 5 "MAX6818EAP+T" H 4700 3400 50  0001 C CNN "Mfr. No. "
	1    4700 3400
	1    0    0    -1  
$EndComp
Text Label 4300 3525 2    50   ~ 0
main_sig
Text Label 4300 3225 2    50   ~ 0
fire_sig
Text Label 4300 3150 2    50   ~ 0
fill_sig
Text Label 4300 3075 2    50   ~ 0
vc_sig
Text HLabel 5125 3525 2    50   Input ~ 0
main_signal
Text HLabel 5125 3300 2    50   Input ~ 0
approach_signal
Text HLabel 5125 3225 2    50   Input ~ 0
fire_signal
Text HLabel 5125 3075 2    50   Input ~ 0
vc_signal
Text HLabel 5125 3150 2    50   Input ~ 0
fill_signal
Text Notes 6975 3025 0    50   ~ 0
Internal pull-up resistors \nare used in the RPi 
Wire Notes Line
	6975 2875 6975 3025
Wire Notes Line
	6975 3025 7975 3025
Wire Notes Line
	7975 3025 7975 2875
Wire Notes Line
	7975 2875 6975 2875
Text Label 4400 3600 2    50   ~ 0
reserved_sig
$Comp
L Device:R R?
U 1 1 61DB4051
P 7300 3225
AR Path="/60723812/61DB4051" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB4051" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB4051" Ref="R15"  Part="1" 
F 0 "R15" V 7250 2975 50  0000 L CNN
F 1 "1k" V 7300 3175 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3225 50  0001 C CNN
F 3 "~" H 7300 3225 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3225 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3225 50  0001 C CNN "Mfr. No. "
	1    7300 3225
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB65EA
P 7300 3325
AR Path="/60723812/61DB65EA" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB65EA" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB65EA" Ref="R16"  Part="1" 
F 0 "R16" V 7250 3075 50  0000 L CNN
F 1 "1k" V 7300 3275 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3325 50  0001 C CNN
F 3 "~" H 7300 3325 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3325 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3325 50  0001 C CNN "Mfr. No. "
	1    7300 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB69FB
P 7300 3425
AR Path="/60723812/61DB69FB" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB69FB" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB69FB" Ref="R17"  Part="1" 
F 0 "R17" V 7250 3175 50  0000 L CNN
F 1 "1k" V 7300 3375 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3425 50  0001 C CNN
F 3 "~" H 7300 3425 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3425 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3425 50  0001 C CNN "Mfr. No. "
	1    7300 3425
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB6CCE
P 7300 3525
AR Path="/60723812/61DB6CCE" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB6CCE" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB6CCE" Ref="R18"  Part="1" 
F 0 "R18" V 7250 3275 50  0000 L CNN
F 1 "1k" V 7300 3475 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3525 50  0001 C CNN
F 3 "~" H 7300 3525 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3525 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3525 50  0001 C CNN "Mfr. No. "
	1    7300 3525
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB6F8A
P 7300 3625
AR Path="/60723812/61DB6F8A" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB6F8A" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB6F8A" Ref="R19"  Part="1" 
F 0 "R19" V 7250 3375 50  0000 L CNN
F 1 "1k" V 7300 3575 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3625 50  0001 C CNN
F 3 "~" H 7300 3625 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3625 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3625 50  0001 C CNN "Mfr. No. "
	1    7300 3625
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB7223
P 7300 3725
AR Path="/60723812/61DB7223" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB7223" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB7223" Ref="R20"  Part="1" 
F 0 "R20" V 7250 3475 50  0000 L CNN
F 1 "1k" V 7300 3675 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3725 50  0001 C CNN
F 3 "~" H 7300 3725 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3725 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3725 50  0001 C CNN "Mfr. No. "
	1    7300 3725
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB7585
P 7300 3825
AR Path="/60723812/61DB7585" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB7585" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB7585" Ref="R21"  Part="1" 
F 0 "R21" V 7250 3575 50  0000 L CNN
F 1 "1k" V 7300 3775 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3825 50  0001 C CNN
F 3 "~" H 7300 3825 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3825 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3825 50  0001 C CNN "Mfr. No. "
	1    7300 3825
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61DB78BA
P 7300 3925
AR Path="/60723812/61DB78BA" Ref="R?"  Part="1" 
AR Path="/6102AF87/61DB78BA" Ref="R?"  Part="1" 
AR Path="/607385D5/61DB78BA" Ref="R22"  Part="1" 
F 0 "R22" V 7250 3675 50  0000 L CNN
F 1 "1k" V 7300 3875 47  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3925 50  0001 C CNN
F 3 "~" H 7300 3925 50  0001 C CNN
F 4 "ROHM Semiconductor" H 7300 3925 50  0001 C CNN "Mfr. "
F 5 "ESR03EZPF1002" H 7300 3925 50  0001 C CNN "Mfr. No. "
	1    7300 3925
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 3225 7150 3225
Wire Wire Line
	7450 3225 7500 3225
Wire Wire Line
	7450 3325 7500 3325
Wire Wire Line
	7000 3425 7150 3425
Wire Wire Line
	7000 3325 7150 3325
Wire Wire Line
	7000 3525 7150 3525
Wire Wire Line
	7000 3625 7150 3625
Wire Wire Line
	7000 3725 7150 3725
Wire Wire Line
	7000 3825 7150 3825
Wire Wire Line
	7000 3925 7150 3925
Wire Wire Line
	7450 3925 7500 3925
Wire Wire Line
	7500 3825 7450 3825
Wire Wire Line
	7500 3725 7450 3725
Wire Wire Line
	7500 3625 7450 3625
Wire Wire Line
	7500 3525 7450 3525
Wire Wire Line
	7500 3425 7450 3425
Text HLabel 5125 3600 2    50   Input ~ 0
res_signal
Wire Wire Line
	5125 3600 5025 3600
Text Label 7000 3525 2    50   ~ 0
abort_sig
Text Label 7000 3625 2    50   ~ 0
approach_sig
Text Label 7000 3425 2    50   ~ 0
drogue_sig
Text Label 7000 3325 2    50   ~ 0
main_sig
Text Label 7000 3725 2    50   ~ 0
fire_sig
Text Label 7000 3825 2    50   ~ 0
fill_sig
Text Label 7000 3925 2    50   ~ 0
vc_sig
Text Label 7000 3225 2    50   ~ 0
reserved_sig
$EndSCHEMATC
