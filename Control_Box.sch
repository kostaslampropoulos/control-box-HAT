EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2875 3900 1300 800 
U 60723812
F0 "POWER SYSTEM" 50
F1 "power_system.sch" 50
$EndSheet
$Sheet
S 6025 3900 1300 800 
U 607385D5
F0 "Inputs " 50
F1 "LEDs_n_switches.sch" 50
F2 "vc_signal" O L 6025 4100 50 
F3 "fill_signal" O L 6025 4175 50 
F4 "fire_signal" O L 6025 4250 50 
F5 "approach_signal" O L 6025 4325 50 
F6 "drogue_signal" O L 6025 4400 50 
F7 "main_signal" O L 6025 4475 50 
F8 "abort_signal" O L 6025 4550 50 
F9 "res_signal" O L 6025 4625 50 
$EndSheet
Text Notes 5700 2100 0    50   ~ 0
COMMS is connected to RPi externally, \nvia USB
Wire Notes Line
	5650 1900 5650 2150
Wire Notes Line
	5650 2150 7200 2150
Wire Notes Line
	7200 2150 7200 1900
Wire Notes Line
	7200 1900 5650 1900
$Comp
L power:GND #PWR?
U 1 1 610298A4
P 5375 2900
AR Path="/6075F4AB/610298A4" Ref="#PWR?"  Part="1" 
AR Path="/610298A4" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 5375 2650 50  0001 C CNN
F 1 "GND" H 5380 2727 50  0000 C CNN
F 2 "" H 5375 2900 50  0001 C CNN
F 3 "" H 5375 2900 50  0001 C CNN
	1    5375 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5275 2850 5275 2900
Wire Wire Line
	5275 2900 5375 2900
$Comp
L power:+3V3 #PWR?
U 1 1 610298AC
P 4250 2075
AR Path="/6075F4AB/610298AC" Ref="#PWR?"  Part="1" 
AR Path="/610298AC" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 4250 1925 50  0001 C CNN
F 1 "+3V3" H 4265 2248 50  0000 C CNN
F 2 "" H 4250 2075 50  0001 C CNN
F 3 "" H 4250 2075 50  0001 C CNN
	1    4250 2075
	1    0    0    -1  
$EndComp
$Comp
L lsf-kicad:PQ9-Connector PQ1
U 1 1 610298B3
P 5275 2250
AR Path="/610298B3" Ref="PQ1"  Part="1" 
AR Path="/6075F4AB/610298B3" Ref="PQ?"  Part="1" 
F 0 "PQ1" H 5623 2218 50  0000 L CNN
F 1 "PQ9-Connector" H 5623 2127 50  0000 L CNN
F 2 "Control_Box:PQ9-Connector_mod" H 5675 1750 50  0001 C CNN
F 3 "https://libre.space/pq9ish" H 5775 2450 50  0001 C CNN
F 4 "Samtec" H 5275 2250 50  0001 C CNN "Mfr. "
F 5 "SQT-109-03-L-S" H 5275 2250 50  0001 C CNN "Mfr. No. "
	1    5275 2250
	1    0    0    -1  
$EndComp
NoConn ~ 4875 1850
Wire Wire Line
	5750 4100 6025 4100
Wire Wire Line
	6025 4175 5750 4175
Wire Wire Line
	5750 4250 6025 4250
Wire Wire Line
	6025 4325 5750 4325
Wire Wire Line
	5750 4400 6025 4400
Wire Wire Line
	5750 4475 6025 4475
Wire Wire Line
	6025 4550 5750 4550
$Sheet
S 4450 3900 1300 800 
U 607284F0
F0 "RPi, programmable LEDs" 50
F1 "RPi_4.sch" 50
F2 "vc_signal" I R 5750 4100 50 
F3 "fill_signal" I R 5750 4175 50 
F4 "fire_signal" I R 5750 4250 50 
F5 "approach_signal" I R 5750 4325 50 
F6 "drogue_signal" I R 5750 4400 50 
F7 "main_signal" I R 5750 4475 50 
F8 "abort_signal" I R 5750 4550 50 
F9 "res_signal" I R 5750 4625 50 
$EndSheet
NoConn ~ 4875 2250
NoConn ~ 4875 2350
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 61DAE702
P 4575 2450
AR Path="/60723812/61DAE702" Ref="JP?"  Part="1" 
AR Path="/61DAE702" Ref="JP4"  Part="1" 
F 0 "JP4" H 4575 2550 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4550 2350 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 4575 2450 50  0001 C CNN
F 3 "~" H 4575 2450 50  0001 C CNN
	1    4575 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4725 2450 4875 2450
Wire Wire Line
	4700 2150 4875 2150
Wire Wire Line
	4250 2450 4425 2450
$Comp
L power:+5V #PWR?
U 1 1 61B8ACBD
P 4250 2450
AR Path="/60723812/61B8ACBD" Ref="#PWR?"  Part="1" 
AR Path="/6102AF87/61B8ACBD" Ref="#PWR?"  Part="1" 
AR Path="/61B8ACBD" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 4250 2300 50  0001 C CNN
F 1 "+5V" H 4325 2550 50  0000 C CNN
F 2 "" H 4250 2450 50  0001 C CNN
F 3 "" H 4250 2450 50  0001 C CNN
	1    4250 2450
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 61DAF3AB
P 4550 2150
AR Path="/60723812/61DAF3AB" Ref="JP?"  Part="1" 
AR Path="/61DAF3AB" Ref="JP3"  Part="1" 
F 0 "JP3" H 4550 2250 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4425 2050 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 4550 2150 50  0001 C CNN
F 3 "~" H 4550 2150 50  0001 C CNN
	1    4550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2075 4250 2150
Wire Wire Line
	4250 2150 4400 2150
Wire Wire Line
	5750 4625 6025 4625
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 61DAD091
P 4250 1700
AR Path="/60723812/61DAD091" Ref="J?"  Part="1" 
AR Path="/61DAD091" Ref="J7"  Part="1" 
F 0 "J7" H 4250 1425 50  0000 C CNN
F 1 "JST PH 1X2" H 4250 1500 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 4250 1700 50  0001 C CNN
F 3 "~" H 4250 1700 50  0001 C CNN
	1    4250 1700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4675 2050 4875 2050
Wire Wire Line
	4450 1600 4675 1600
Wire Wire Line
	4675 1600 4675 2050
Wire Wire Line
	4875 1950 4575 1950
Wire Wire Line
	4575 1950 4575 1700
Wire Wire Line
	4575 1700 4450 1700
$EndSCHEMATC
